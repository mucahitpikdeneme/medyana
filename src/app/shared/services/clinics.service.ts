import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({ providedIn: 'root' })
export class ClinicsService {

  private apiUrl = environment.apiUrl + '?element=clinics';

  constructor(
    private httpClient: HttpClient
  ) {
  }

  getClinic(id) {

    let httpParams = new HttpParams();
    httpParams = httpParams.append('id', id);

    return this.httpClient.get(this.apiUrl, { observe: 'response', params: httpParams});
  }

  getClinics(searchValue = null, pageIndex = null, rowsLimit = null, sortBy = null, sortType = null) {

    let httpParams = new HttpParams();
    if (searchValue) httpParams = httpParams.append('searchValue', searchValue);
    if (pageIndex) httpParams = httpParams.append('pageIndex', pageIndex);
    if (rowsLimit) httpParams = httpParams.append('rowsLimit', rowsLimit);
    if (sortBy) httpParams = httpParams.append('sortBy', sortBy);
    if (sortType) httpParams = httpParams.append('sortType', sortType);

    return this.httpClient.get(this.apiUrl, { observe: 'response', params: httpParams});

  }

  addClinic(title) {

    let httpParams = new HttpParams();
    httpParams = httpParams.append('title', title);

    // return this.httpClient.get(this.apiUrl, {params: httpParams, observe: 'response'});
    return this.httpClient.post(this.apiUrl, httpParams,{ observe: 'response'});
  }

  updateClinic(id, title) {

    let httpParams = new HttpParams();
    httpParams = httpParams.append('id', id);
    httpParams = httpParams.append('title', title);

    // return this.httpClient.get(this.apiUrl, {params: httpParams, observe: 'response'});
    return this.httpClient.put(this.apiUrl, httpParams,{ observe: 'response'});
  }

  deleteClinic(id) {

    let httpParams = new HttpParams();
    httpParams = httpParams.append('id', id);

    return this.httpClient.delete(this.apiUrl, { observe: 'response', params: httpParams});
  }

}
