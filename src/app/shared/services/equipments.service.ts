import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({ providedIn: 'root' })
export class EquipmentsService {

  private apiUrl = environment.apiUrl2 + '?element=equipments';

  constructor(
    private httpClient: HttpClient
  ) {
  }

  getEquipment(id) {

    let httpParams = new HttpParams();
    httpParams = httpParams.append('id', id);

    return this.httpClient.get(this.apiUrl, { observe: 'response', params: httpParams});
  }

  getEquipments(searchValue = null, pageIndex = null, rowsLimit = null, sortBy = null, sortType = null) {

    let httpParams = new HttpParams();
    if (searchValue) httpParams = httpParams.append('searchValue', searchValue);
    if (pageIndex) httpParams = httpParams.append('pageIndex', pageIndex);
    if (rowsLimit) httpParams = httpParams.append('rowsLimit', rowsLimit);
    if (sortBy) httpParams = httpParams.append('sortBy', sortBy);
    if (sortType) httpParams = httpParams.append('sortType', sortType);

    return this.httpClient.get(this.apiUrl, { observe: 'response', params: httpParams});

  }

  addEquipments(title = null, date = null, quantity = null
    , unit_price = null, usage_percentage = null, clinic_ids = null) {

    let httpParams = new HttpParams();
    httpParams = httpParams.append('title', title);
    httpParams = httpParams.append('date', date);
    httpParams = httpParams.append('quantity', quantity);
    httpParams = httpParams.append('unit_price', unit_price);
    httpParams = httpParams.append('usage_percentage', usage_percentage);
    httpParams = httpParams.append('clinic_ids', clinic_ids);

    // return this.httpClient.get(this.apiUrl, {params: httpParams, observe: 'response'});
    return this.httpClient.post(this.apiUrl, httpParams,{ observe: 'response'});
  }

  updateEquipments(id = null, title = null, date = null, quantity = null
                   , unit_price = null, usage_percentage = null, clinic_ids = null) {

    let httpParams = new HttpParams();
    httpParams = httpParams.append('id', id);
    httpParams = httpParams.append('title', title);
    httpParams = httpParams.append('date', date);
    httpParams = httpParams.append('quantity', quantity);
    httpParams = httpParams.append('unit_price', unit_price);
    httpParams = httpParams.append('usage_percentage', usage_percentage);
    httpParams = httpParams.append('clinic_ids', clinic_ids);

    return this.httpClient.put(this.apiUrl, httpParams,{ observe: 'response'});
  }

  deleteEquipments(id) {

    let httpParams = new HttpParams();
    httpParams = httpParams.append('id', id);

    return this.httpClient.delete(this.apiUrl, { observe: 'response', params: httpParams});
  }

}
