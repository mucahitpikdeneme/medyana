import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EquipmentsRoutingModule} from './equipments-routing.module';
import {EquipmentsComponent} from "./equipments.component";
import {FormsModule} from "@angular/forms";
import {TableComponent} from "./table/table.component";
import {AddEditComponent} from "./addEdit/add-edit.component";


@NgModule({
  declarations: [
    EquipmentsComponent,
    AddEditComponent,
    TableComponent,
  ],
  imports: [
    CommonModule,
    EquipmentsRoutingModule,
    FormsModule,
  ]
})
export class EquipmentsModule { }


