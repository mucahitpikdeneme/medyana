import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {EquipmentsService} from "../shared/services/equipments.service";
import {TableComponent} from "./table/table.component";

@Component({
  selector: 'app-equipments',
  templateUrl: './equipments.component.html',
  styleUrls: ['./equipments.component.scss']
})
export class EquipmentsComponent implements AfterViewInit {

  @ViewChild(TableComponent, { static: false }) tableComponent: TableComponent;


  timer: any;
  success = '';

  tableColums = [
    { title: 'Equipments\'s Id', value: 'id', sortState: 0 },
    { title: 'Equipments\'s Name', value: 'name', sortState: 0 },
    { title: 'Date', value: 'date', sortState: 0 },
    { title: 'Quantity', value: 'quantity', sortState: 0 },
    { title: 'Unit Price', value: 'unit_price', sortState: 0 },
    { title: 'Usage Percentage', value: 'usage_percentage', sortState: 0 },
    { title: 'Clinics', value: 'clinic_titles', sortState: 0 },
    { title: '', value: 'edit', field: 'id', plus: '/edit' },
  ];


  tableGetValuesOrg: any = {
    pageIndex: 1,
    rowsLimit: 10,
    numberOfPages: 10,
    searchValue: null,
    sortBy: null,
    sortType: null,
  };
  tableGetValues: any = {...this.tableGetValuesOrg};

  constructor(
    private equipmentsService: EquipmentsService,
  ) { }

  ngAfterViewInit() {
    this.getEquipments();
  }

  getEquipments() {
    this.equipmentsService.getEquipments(this.tableGetValues.searchValue
      , this.tableGetValues.pageIndex, this.tableGetValues.rowsLimit
      , this.tableGetValues.sortBy, this.tableGetValues.sortType )
      .subscribe(res => {
        const inTotal = res.headers.get('inTotal');
        const equipments: any = res.body;
        this.tableComponent.setData(equipments, this.tableGetValues.pageIndex
          , this.tableGetValues.rowsLimit, inTotal);
      });
  }

  onRefreshTableDate(obj: any) {
    if (obj.hasOwnProperty('pageIndex')) this.tableGetValues.pageIndex = obj.pageIndex;
    if (obj.hasOwnProperty('rowsLimit')) this.tableGetValues.rowsLimit = obj.rowsLimit;
    if (obj.hasOwnProperty('searchValue')) this.tableGetValues.searchValue = obj.searchValue;
    if (obj.hasOwnProperty('sortBy')) this.tableGetValues.sortBy = obj.sortBy;
    if (obj.hasOwnProperty('sortType')) this.tableGetValues.sortType = obj.sortType;

    this.getEquipments();
  }

  onDeleteRow(id: number) {
    let r = confirm("Do you confirm to delete? id:" + id);

    if (r) {
      this.equipmentsService.deleteEquipments(id)
        .subscribe(res => {
          this.success = " Clinic #" + id + " was deleted";
          this.tableGetValues = {...this.tableGetValuesOrg};
          this.getEquipments();
          clearTimeout(this.timer);
          this.timer = setTimeout(() => {
            this.success = '';
          }, 1000);
        });
    }
  }

}
