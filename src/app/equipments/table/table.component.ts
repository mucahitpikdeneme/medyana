import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
})
export class TableComponent implements OnInit {


  @Output() refreshTableDateEE = new EventEmitter<any>();
  @Output() deleteRowEE = new EventEmitter<number>();
  @Input() tableColums: any;

  // tableColums: any = [
    // { title: 'Clinic\'s Name', value: 'name', sortState: 0 },
    // { title: '', value: 'edit', sortState: -1, field: 'id', plus: '/edit' },
  // ];

  paginationValues: any = [
    { value: null, disabled: false },
    { value: 1, disabled: false },
    { value: 2, disabled: false },
    { value: 3, disabled: false },
    { value: 4, disabled: false },
    { value: 5, disabled: false },
    { value: null, disabled: false },
  ];
  tableGetValues: any = {
    // activePaginationIndex: 3,
    pageIndex: 1,
    rowsLimit: 10,
    numberOfPages: 10,
    searchValue: null,
    sortBy: null,
    sortType: null,
  };

  isRloDropOpen = false;

  rowsLimitsOptions = [
    10,
    20,
    30,
  ];

  tableData: any = [
    { id: 1, name: 'Clinic A' },
  ];

  foundTotal = null;
  noResultsColSpan = 0;

  constructor() { }

  ngOnInit() {
  }

  onToggleRowsLimits() {
    this.isRloDropOpen = !this.isRloDropOpen;
  }

  onChangeRowsLimitsOptions(item) {
    this.tableGetValues.rowsLimit = item;
    this.onToggleRowsLimits();
    this.refreshTableDateEE.emit({ rowsLimit: item, pageIndex: 1 });
  }

  onSortChange(columnValue) {

    if (columnValue === 'edit') return;

    this.tableColums.map(c => {
      if (c.hasOwnProperty('sortState') && c.value !== columnValue) c.sortState = 0;
    });

    const foundColumn = this.tableColums.find(c => c.value === columnValue );
    let sortStateToBeAssigned;
    if (foundColumn.sortState === 0) { sortStateToBeAssigned = 1; }
    if (foundColumn.sortState === 1) { sortStateToBeAssigned = 2; }
    if (foundColumn.sortState === 2) { sortStateToBeAssigned = 0; }

    this.tableColums.find(c => c.value === columnValue ).sortState = sortStateToBeAssigned;

    this.tableGetValues.sortBy = columnValue;
    if (sortStateToBeAssigned === 1) this.tableGetValues.sortType = 'asc';
    if (sortStateToBeAssigned === 2) this.tableGetValues.sortType = 'desc';
    if (sortStateToBeAssigned === 0) {
      this.tableGetValues.sortBy = null;
      this.tableGetValues.sortType = null;
    }
    this.refreshTableDateEE.emit({
      sortBy: this.tableGetValues.sortBy, sortType: this.tableGetValues.sortType
    });
  }

  onDeleteRow(id: number) {
    this.deleteRowEE.emit(id);
  }

  onChangePage(page) {
    if (page === -1) { page = this.tableGetValues.pageIndex - 1; }
    else if (page === -2) { page = this.tableGetValues.pageIndex + 1; }
    this.refreshTableDateEE.emit({ pageIndex: page });
  }

  onInputSearchbox() {
    if (this.tableGetValues.searchValue) {

      this.tableGetValues.sortBy = null;
      this.tableGetValues.sortType = null;

      this.refreshTableDateEE.emit({
        searchValue: this.tableGetValues.searchValue,
        pageIndex: 1,
        rowsLimit: 10,
        sortBy: this.tableGetValues.sortBy,
        sortType: this.tableGetValues.sortType
      });
    }
  }

  onClearSearchbox() {
    this.tableGetValues.searchValue = '';
    this.refreshTableDateEE.emit({
      searchValue: this.tableGetValues.searchValue,
      pageIndex: 1,
      rowsLimit: 10,
    });
  }

  setData(data = null,pageIndex = null,rowsLimit = null
          , foundTotal = null,searchValue = null,) {
    if (pageIndex !== null)  this.tableGetValues.pageIndex = pageIndex;
    if (rowsLimit !== null)  this.tableGetValues.rowsLimit = rowsLimit;
    if (foundTotal !== null)  this.foundTotal = foundTotal;
    if (data !== null)  this.tableData = data;

    this.tableGetValues.numberOfPages =
      Math.ceil(this.foundTotal / this.tableGetValues.rowsLimit);

    this.handlePagination();
  }

  handlePagination() {

    const lastPage = this.tableGetValues.numberOfPages;
    const activePage = this.tableGetValues.pageIndex;
    if (activePage > 3) {
      this.paginationValues[1].value = activePage - 2;
      this.paginationValues[2].value = activePage - 1;
      this.paginationValues[3].value = activePage;
      this.paginationValues[4].value = activePage + 1;
      this.paginationValues[5].value = activePage + 2;
    } else if (activePage <= 3) {
      this.paginationValues[1].value = 1;
      this.paginationValues[2].value = 2;
      this.paginationValues[3].value = 3;
      this.paginationValues[4].value = 4;
      this.paginationValues[5].value = 5;
    }

    if (lastPage >= this.paginationValues[1].value) {
      this.paginationValues[1].disabled = false;
    } else {
      this.paginationValues[1].disabled = true;
    }
    if (lastPage >= this.paginationValues[2].value) {
      this.paginationValues[2].disabled = false;
    } else {
      this.paginationValues[2].disabled = true;
    }
    if (lastPage >= this.paginationValues[3].value) {
      this.paginationValues[3].disabled = false;
    } else {
      this.paginationValues[3].disabled = true;
    }
    if (lastPage >= this.paginationValues[4].value) {
      this.paginationValues[4].disabled = false;
    } else {
      this.paginationValues[4].disabled = true;
    }
    if (lastPage >= this.paginationValues[5].value) {
      this.paginationValues[5].disabled = false;
    } else {
      this.paginationValues[5].disabled = true;
    }

    if (lastPage > activePage) {
      this.paginationValues[6].disabled = false;
    } else {
      this.paginationValues[6].disabled = true;
    }

    if (activePage > 1) {
      this.paginationValues[0].disabled = false;
    } else {
      this.paginationValues[0].disabled = true;
    }

  }

}
