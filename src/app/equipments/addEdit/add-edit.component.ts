import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Data, Router} from "@angular/router";
import {EquipmentsService} from "../../shared/services/equipments.service";
import {ClinicsService} from "../../shared/services/clinics.service";

@Component({
  selector: 'app-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.scss']
})
export class AddEditComponent implements OnInit {

  add = false;
  success = '';
  error = '';

  title = '';
  date = null;
  quantity = null;
  unit_price = null;
  usage_percentage = null;
  clinic_ids = '';

  loading = false;

  equipment: any = {
    id: null,
    title: null,
  };

  searchedClinics: any = [
    // { id: 1, text:''},
  ];

  addedClinics: any = [
    // { id: 1, text:''},
  ];

  secOps: any = {
    dp: false,
    selectedId: null,
    itxtValue: '',
  };

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private equipmentsService: EquipmentsService,
    private clinicsService: ClinicsService,
  ) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data: Data) => {
      if (data['add'] === true) { this.add = true;  }
      if (!this.add) {
        const id = +this.activatedRoute.snapshot.params['id'];
        this.equipmentsService.getEquipment(id).subscribe(c => {
            const body: any = c.body;
            this.equipment = body;
            this.equipment.title = body.name;
            this.title = body.name;
            this.date = body.date;
            this.quantity = body.quantity;
            this.unit_price = body.unit_price;
            this.usage_percentage = body.usage_percentage;
            this.clinic_ids = body.clinic_ids;
            let clinic_idsAr = body.clinic_ids.split(',');
            let clinic_titlesAr = body.clinic_titles.split(',');
            clinic_idsAr.map((i, index) => {
              this.addedClinics.push({ id: i, text: clinic_titlesAr[index] });
            });
        });
      }
    });

  }

  onAddEdit() {
    if (this.title.length < 3) {
      this.error = 'Equipment\'s title must include more than 3 characters';
      return;
    }
    else if (this.quantity.length === 0) {
      this.error = 'Quantity must be filled';
      return;
    }
    else if (this.quantity < 1) {
      this.error = 'Quantity must be bigger than 0';
      return;
    }
    else if (this.unit_price.length && parseFloat(this.unit_price) < 0.01) {
      this.error = 'Price must be bigger than 0.01';
      return;
    }
    else if (this.clinic_ids.length === 0) {
      this.error = 'At least one clinic must be selected ';
      return;
    }
    else {
      this.error = "";
    }

    this.loading = true;

    if (this.add) {
      this.equipmentsService.addEquipments(this.title, this.date
        , this.quantity, this.unit_price, this.usage_percentage, this.clinic_ids)
        .subscribe(res => {
          const body: any = res.body;
          this.success = "Equipment #" + body.id + " '" + this.title + "' successfully created";
          this.loading = false;
          setTimeout(() => {
            this.router.navigate(['../' + body.id + '/edit'], { relativeTo: this.activatedRoute});
          }, 1000);
        });
    } else {
      this.equipmentsService.updateEquipments(this.equipment.id, this.title, this.date
        , this.quantity, this.unit_price, this.usage_percentage, this.clinic_ids)
        .subscribe(res => {
          const body: any = res.body;
          this.success = "Equipment #" + body.id + " '" + body.name + "' successfully saved";
          this.equipment.title = body.name;
          this.loading = false;
        });
    }
  }


  onFocusSC() {
    this.secOps.dp = true;
  }

  onBlurSC() {
    setTimeout(() => {
      this.secOps.dp = false;
    }, 100);
  }

  onTypeSC(event) {
    const value = event.target.value;
    this.clinicsService.getClinics(value, 1, 5)
      .subscribe(res => {
        const body: any = res.body;
        this.searchedClinics.length = 0;
        body.map(c => { this.searchedClinics.push({ id: c.id, text: c.name }); });
        console.log(this.searchedClinics);
      });
  }

  onSelectSC(value: any) {
    const founcC = this.searchedClinics.find(c => c.id === value);
    this.secOps.itxtValue = founcC.text;
    this.secOps.selectedId = founcC.id;
  }

  onAddSC() {
    (this.secOps.selectedId);
    const founcC = this.searchedClinics.find(c => c.id === this.secOps.selectedId);
    this.addedClinics.push({ id: founcC.id, text: founcC.text });
    this.addToClinicIds(this.secOps.selectedId);
    this.secOps.selectedId = null;
    this.secOps.itxtValue = '';

  }

  onRemoveSC(value: any) {
    this.addedClinics = this.addedClinics.filter(c => c.id !== value);
    this.removeFromClinicIds(value);
  }

  addToClinicIds(id) {
    this.clinic_ids += ',' + id;
    if (this.clinic_ids.charAt(0) === ',') { this.clinic_ids = this.clinic_ids.substr(1);  }
  }

  removeFromClinicIds(id) {
    let ar = this.clinic_ids.split(',');
    ar = ar.filter(a => a !== id);
    this.clinic_ids = ar.join(',');
  }

}
