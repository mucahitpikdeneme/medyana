import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

const routes = [
  { path: '', redirectTo: 'clinics', pathMatch: 'full' },
  { path: 'clinics', loadChildren: './clinics/clinics.module#ClinicsModule' },
  { path: 'equipments', loadChildren: './equipments/equipments.module#EquipmentsModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
