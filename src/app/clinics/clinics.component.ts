import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ClinicsService} from "../shared/services/clinics.service";
import {TableComponent} from "./table/table.component";

@Component({
  selector: 'app-clinics',
  styleUrls: ['./clinics.component.scss'],
  templateUrl: './clinics.component.html',
})
export class ClinicsComponent implements AfterViewInit {

  @ViewChild(TableComponent, { static: false }) tableComponent: TableComponent;

  timer: any;
  success = '';

  tableColums = [
    { title: 'Clinic\'s Id', value: 'id', sortState: 0 },
    { title: 'Clinic\'s Name', value: 'name', sortState: 0 },
    { title: '', value: 'edit', field: 'id', plus: '/edit' },
  ];


  tableGetValuesOrg: any = {
    pageIndex: 1,
    rowsLimit: 10,
    numberOfPages: 10,
    searchValue: null,
    sortBy: null,
    sortType: null,
  };
  tableGetValues: any = {...this.tableGetValuesOrg};

  constructor(
    private clinicsService: ClinicsService
  ) { }

  ngAfterViewInit() {
    this.getClinics();
  }

  getClinics() {
    this.clinicsService.getClinics(this.tableGetValues.searchValue
      , this.tableGetValues.pageIndex, this.tableGetValues.rowsLimit
      , this.tableGetValues.sortBy, this.tableGetValues.sortType )
      .subscribe(res => {
        const inTotal = res.headers.get('inTotal');
        const clinics: any = res.body;
        this.tableComponent.setData(clinics, this.tableGetValues.pageIndex
          , this.tableGetValues.rowsLimit, inTotal);
      });
  }

  onRefreshTableDate(obj: any) {
    if (obj.hasOwnProperty('pageIndex')) this.tableGetValues.pageIndex = obj.pageIndex;
    if (obj.hasOwnProperty('rowsLimit')) this.tableGetValues.rowsLimit = obj.rowsLimit;
    if (obj.hasOwnProperty('searchValue')) this.tableGetValues.searchValue = obj.searchValue;
    if (obj.hasOwnProperty('sortBy')) this.tableGetValues.sortBy = obj.sortBy;
    if (obj.hasOwnProperty('sortType')) this.tableGetValues.sortType = obj.sortType;

    this.getClinics();
  }

  onDeleteRow(id: number) {
    let r = confirm("Do you confirm to delete? id:" + id);

    if (r) {
      this.clinicsService.deleteClinic(id)
        .subscribe(res => {
        this.success = " Clinic #" + id + " was deleted";
        this.tableGetValues = {...this.tableGetValuesOrg};
        this.getClinics();
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
          this.success = '';
        }, 1000);
      });
    }
  }

}
