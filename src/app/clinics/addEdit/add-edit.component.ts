import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Data, Params, Route, Router} from "@angular/router";
import {ClinicsService} from "../../shared/services/clinics.service";

@Component({
  selector: 'app-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.scss']
})
export class AddEditComponent implements OnInit {

  add = false;
  success = '';
  error = '';
  title = '';
  loading = false;

  clinic: any = {
    id: null,
    title: null,
  };

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private clinicsService: ClinicsService,
  ) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data: Data) => {
      if (data['add'] === true) { this.add = true;  }
      if (!this.add) {
        const id = +this.activatedRoute.snapshot.params['id'];
        this.clinicsService.getClinic(id).subscribe(c => {
            const body: any = c.body;
            this.clinic = body;
            this.clinic.title = body.name;
            this.title = body.name;
        });
      }
    });
  }

  onAddEdit() {
    if (this.title.length < 3) {
      this.error = 'Clinic\'s title must include more than 3 characters';
      return;
    }
    else {
      this.error = "";
    }

    this.loading = true;

    if (this.add) {
      this.clinicsService.addClinic(this.title)
        .subscribe(res => {
          const body: any = res.body;
          this.success = "Clinic #" + body.id + " '" + this.title + "' successfully created";
          this.loading = false;
          setTimeout(() => {
            this.router.navigate(['../' + body.id + '/edit'], { relativeTo: this.activatedRoute});
          }, 1000);
        });
    } else {
      this.clinicsService.updateClinic(this.clinic.id, this.title)
        .subscribe(res => {
          const body: any = res.body;
          this.success = "Clinic #" + body.id + " '" + body.name + "' successfully saved";
          this.clinic.title = body.name;
          this.loading = false;
        });
    }
  }

}
