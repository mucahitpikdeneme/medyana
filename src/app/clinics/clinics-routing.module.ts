import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClinicsComponent} from "./clinics.component";
import {AddEditComponent} from "./addEdit/add-edit.component";


const routes: Routes = [
  { path: '', component: ClinicsComponent },
  { path: 'add', component: AddEditComponent, data: { add: true }},
  { path: ':id/edit', component: AddEditComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClinicsRoutingModule { }
