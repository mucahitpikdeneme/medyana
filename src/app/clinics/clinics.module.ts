import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClinicsRoutingModule} from './clinics-routing.module';
import {ClinicsComponent} from "./clinics.component";
import {FormsModule} from "@angular/forms";
import {AddEditComponent} from "./addEdit/add-edit.component";
import {HttpClientModule} from "@angular/common/http";
import {TableComponent} from "./table/table.component";


@NgModule({
  declarations: [
    ClinicsComponent,
    AddEditComponent,
    TableComponent
  ],
  imports: [
    CommonModule,
    ClinicsRoutingModule,
    FormsModule,
    HttpClientModule,
  ]
})
export class ClinicsModule { }

